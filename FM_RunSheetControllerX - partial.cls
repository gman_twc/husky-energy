/*************************************************************************************************\
Author: 		Gui Manders
Company: 		Husky Energy
Description:	Controller Extension for Runsheet Functionality
**************************************************************************************************/

public class FM_RunSheetControllerX
{
	// ********************************************************************************* //
	// ** THIS CONTROLLER EXTENSION IS USED FOR THE MAJORITY OF FLUID RUN SHEET LOGIC ** //
	// ********************************************************************************* //

	///////////////////////////
	// * FLUID OBJECT DATA * //
	///////////////////////////

	public List<RunsheetWrapper> lWrapperList {get; private set;}
	public List<RunsheetWrapper> lExtraWrapperList {get; private set;}
	public List<FM_Run_Sheet__c> lYesterdayRunSheetList {get; private set;}

	public FM_Run_Sheet__c oRunSheet {get; private set;}
	public Integer iLineChanged {get; set;}

	public String sRunsheetId {get; private set;}
	public Boolean bIsViewMode {get; private set;}

	//////////////////////////////////////////
	// * CONFIGURATION & PERMISSIONS DATA * //
	//////////////////////////////////////////

	// This is used for Load Weight Configurations
	public Integer[] aLoadAmounts {get; private set;}

	// URL Parameters - Used for Dispatch View search criteria
	public String sUrlParams {get; private set;}

	public Boolean bFLOCH2S {get; set;}
	public Boolean bCanViewServiceWork {get; private set;}
	public Boolean bCanSubmitSand {get; private set;}
	public Boolean bEditFromDispatchView {get; private set;}

	// These columns no longer show up IF all related Tanks have their information filled out
	public Boolean bDisplaySerialNumberColumn {get; private set;}
	public Boolean bDisplayLowLevelColumn {get; private set;}
	public Boolean bDisplayDoorStatusColumn {get; private set;}

	///////////////////////////////
	// * OPERATOR ON CALL DATA * //
	///////////////////////////////

	public Operator_On_Call__c oOperatorOnCall {get; private set;}
	public Boolean bUserIsOperator {get; private set;}

	public String sOperatorOnCallFormattedDate {get; private set;}
	public String sTakeOperatorOnCall {get; set;}

	//////////////////////////
	// * H2S READING DATA * //
	//////////////////////////

	public Well_Location_H2S_Reading__c oNewReading {get; set;}
	public Well_Location_H2S_Reading__c oPreviousReading {get; private set;}

	public String sFormattedReadingTime {get; private set;}

	////////////////////
	// * ROUTE DATA * //
	////////////////////

	public List<SelectOption> lRouteList {get; set;}
	public Route__c oSelectedRoute {get; private set;}

	public String sRouteNum {get; set;}
	public String sRouteId {get; set;}

	private Set<Id> setRoutes;

	///////////////////////
	// * LOCATION DATA * //
	///////////////////////

	private Map<Id, Location__c> mapLocations;
	public List<SelectOption> lLocationList {get; set;}

	public Location__c oSelectedLocation {get; private set;}
	public Location__c oSelectedLocationPvr {get; private set;}

	public String sSelectedLocationId {get; set;}
	public String sLocationType {get; set;}

	///////////////////////
	// * FACILITY DATA * //
	///////////////////////

	public Facility__c oSelectedFacility {get; private set;}
	public String sSelectedFacilityId {get; set;}

	////////////////////////
	// * FAVORITES DATA * //
	////////////////////////

	public SelectOption[] aSelectedWells {get; set;}
	public SelectOption[] aAllWells {get; set;}

	private FM_CustomWellOrder__c oCustomWellOrderData {get; set;}
	public String sSavedSelectedWells {get; set;}

	////////////////////////////////////////////
	// * BOOKING DATA -> PRODUCED VS BOOKED * //
	////////////////////////////////////////////

	public BookingData oTodayBookingData {get; private set;}
	public BookingData oYesterdayBookingData {get; private set;}

	//////////////////////////
	// * MAIN CONSTRUCTOR * //
	//////////////////////////

	public FM_RunSheetControllerX(ApexPages.StandardController stdController)
	{
		SetConfigurationData();
		GetUrlParameters();
		InitializeNewRunsheetData();

		// Creating NEW Runsheet?
		if(this.sRunsheetId == null)
		{
			InitializeLocationData();
			BuildRouteList();
			SelectDefaultRoute();
		}
		// View OR Edit Mode
		else
		{
			this.lWrapperList = RetrieveRunsheetData(this.sRunsheetId);
			this.oRunSheet = (this.lWrapperList.size() >= FM_Utilities.RUNSHEET_SINGLE_RESULT) ? lWrapperList[0].oTodaysRunsheet : null;

			// Run Sheet was found?
			if(this.oRunSheet != null)
			{
				RefreshPreviousH2SReading();
				RetrieveOperatorOnCallData(this.oRunSheet.Well__r.Route__c);
			}
		}
	}

	////////////////////////////////////////////////////////////////////
	// * SET AND INITIALIZE GLOBAL CONFIGURATION & PERMISSIONS DATA * //
	////////////////////////////////////////////////////////////////////

	private void SetConfigurationData()
	{
		this.sTakeOperatorOnCall = '';
		this.sLocationType = 'well';
		this.sRunsheetId = ApexPages.currentPage().getParameters().get('id');
		this.bIsViewMode = (this.sRunsheetId == null || this.sRunsheetId == '') ? false : true;
		this.bEditFromDispatchView = false;

		this.bUserIsOperator = FM_Utilities.VerifyPermissions(UserInfo.getUserId(), FM_Utilities.FLUID_PERMISSION_SET_OPERATOR);
		this.bCanViewServiceWork = FM_Utilities.VerifyPermissions(UserInfo.getUserId(), FM_Utilities.FLUID_PERMISSION_SET_DISPATCH_STANDARD) || FM_Utilities.VerifyPermissions(UserInfo.getUserId(), FM_Utilities.FLUID_PERMISSION_SET_FLUID_ADMIN) || FM_Utilities.VerifyProfile('Standard HOG - Administrator') || FM_Utilities.VerifyProfile('System Administrator');
		this.bCanSubmitSand = FM_Utilities.VerifyPermissions(UserInfo.getUserId(), FM_Utilities.FLUID_PERMISSION_SET_OPERATOR) || FM_Utilities.VerifyPermissions(UserInfo.getUserId(), FM_Utilities.SAND_PERMISSION_SET_SAND_LEAD);
	}

	/////////////////////////////////////////////////////////////////
	// * GET URL PARAMETERS (SEARCH SETTINGS FROM DISPATCH VIEW) * //
	/////////////////////////////////////////////////////////////////

	private void GetUrlParameters()
	{
		Map<String, String> mapParameters = ApexPages.currentPage().getParameters();
		this.sUrlParams = '';

		for(String sOneParam : mapParameters.keySet())
		{
			// IGNORE NON DISPATCH-VIEW PARAMETERS IN URL
			this.sUrlParams += (sOneParam.contains('dv_')) ? sOneParam + '=' + mapParameters.get(sOneParam) + '&' : '';
			this.bEditFromDispatchView = (sOneParam.contains('dv_')) ? true : this.bEditFromDispatchView;
		}
	}

	//////////////////////////////////////
	// * INITIALIZE NEW RUNSHEET DATA * //
	//////////////////////////////////////

	private void InitializeNewRunsheetData()
	{
		this.mapLocations = new Map<Id, Location__c>();
		this.lRouteList = new list<SelectOption>();
		this.lLocationList = new list<SelectOption>();

		this.oRunSheet = new FM_Run_Sheet__c();
		this.oRunSheet.Date__c = Date.Today();

		this.lWrapperList = new List<RunsheetWrapper>();
		this.lExtraWrapperList = new List<RunsheetWrapper>();
		this.lYesterdayRunSheetList = new List<FM_Run_Sheet__c>();
	}

	///////////////////////////////////////////////////
	// * INITIALIZE ALL LOCATION AND FACILITY DATA * //
	///////////////////////////////////////////////////

	private void InitializeLocationData()
	{
		this.lLocationList = new List<SelectOption>();
		this.sTakeOperatorOnCall = '';

		this.oSelectedLocation = null;
		this.sSelectedLocationId = getNotSelected();

		this.oSelectedFacility = null;
		this.sSelectedFacilityId = getNotSelected();
	}

	/////////////////////////////////////
	// * BUILD SELECTABLE ROUTE LIST * //
	/////////////////////////////////////

	private void BuildRouteList()
	{
		List<Equipment_Tank__c> lAllRoutes = [SELECT Equipment__r.Location__r.Route__r.Id, Equipment__r.Location__r.Route__r.Name FROM Equipment_Tank__c WHERE Equipment__r.Location__r.Route__r.Fluid_Management__c = true AND Equipment__r.Location__r.Functional_Location_Category__c IN :FM_Utilities.RUNSHEET_FUNCTIONAL_LOCATION_CATEGORIES AND Equipment__r.Location__r.Status__c IN :FM_Utilities.RUNSHEET_WELL_INCLUDED_STATUSES AND Equipment__r.Location__r.Well_Type__c IN :FM_Utilities.RUNSHEET_WELL_TYPES];
		this.setRoutes = new Set<Id>();

		for(Equipment_Tank__c oOneRoute : lAllRoutes)
		{
			if(!this.setRoutes.contains(oOneRoute.Equipment__r.Location__r.Route__r.Id))
			{
				this.lRouteList.add(new SelectOption(oOneRoute.Equipment__r.Location__r.Route__r.Id, oOneRoute.Equipment__r.Location__r.Route__r.Name));
				this.setRoutes.add(oOneRoute.Equipment__r.Location__r.Route__r.Id);
			}
		}

		// Sort the final Route List
		this.lRouteList = FM_Utilities.SortSelectOptions(this.lRouteList, FM_Utilities.SortSelectOptionBy.Label);
	}

	/////////////////////////////////////////////////////////////////////
	// * SELECT THE DEFAULT ROUTE BASED ON PREVIOUS RUNSHEET ENTERED * //
	/////////////////////////////////////////////////////////////////////

	private void SelectDefaultRoute()
	{
		List<FM_Run_Sheet__c> lLastRunSheet = [SELECT Id, Name, Well__r.Route__c, Well__r.Route__r.Name FROM FM_Run_Sheet__c WHERE Well__c != null AND CreatedById = :UserInfo.getUserId() AND Well__r.Route__c IN :this.setRoutes ORDER BY CreatedDate DESC LIMIT 1];

		if(lLastRunSheet.size() > 0)
		{
			this.sRouteId = lLastRunSheet[0].well__r.Route__c;
			this.sRouteNum = lLastRunSheet[0].well__r.Route__r.Name;

			RefreshLocationList();
		}
	}

	////////////////////////////////////////////////////////////
	// * RETRIEVE RUNSHEET DATA FOR THE SPECIFIED OBJECT ID * //
	////////////////////////////////////////////////////////////

	private List<RunsheetWrapper> RetrieveRunSheetData(String sRunSheetId)
	{
		List<FM_Run_Sheet__c> lRunSheets = [SELECT Id, Date__c, Facility__c, Well__c FROM FM_Run_Sheet__c WHERE Id = :sRunSheetId];
		FM_Run_Sheet__c oFoundRunSheet = (lRunSheets.size() == FM_Utilities.RUNSHEET_SINGLE_RESULT) ? lRunSheets[0] : null;

		List<Equipment_Tank__c> lTankList = new List<Equipment_Tank__c>();
		Map<String, Equipment_Tank__c> mapTanks = new Map<String, Equipment_Tank__c>();

		// Runsheet(s) found?
		if(oFoundRunSheet != null)
		{
			lRunSheets = (oFoundRunSheet.Facility__c != null) ? [SELECT Id, Name, Act_Flow_Rate__c, Act_Tank_Level__c, Business_Unit__c, Comments_Category__c, Current_Well_Status__c, Date__c, Equipment_Tank__c, Equipment_Tank__r.Door_Status__c, Facility__c, Flowline_Volume__c, Fluid_Booked__c, Is_Fluid_Facility__c, Load_Type__c, Load_Weight__c, Operating_District__c, Outstanding_Oil__c, Outstanding_Water__c, Sour__c, Standing_Comments__c, Tank__c, Tank_Label__c, Tank_Size__c, Tomorrow_Oil__c, Tomorrow_Water__c, Tonight_Oil__c, Tonight_Water__c, Total_Loads__c, Well_Name_Tank__c, Well__c, Well__r.Hazardous_H2S__c, Well__r.Route__c, Well__r.Route__r.Field_Senior__c, Well__r.Statusicon__c, Well__r.Unit_Configuration__c, Well__r.Current_Well_Status__c, Well__r.PVR_AVGVOL_30D_OIL__c, Well__r.PVR_AVGVOL_30D_SAND__c, Well__r.PVR_AVGVOL_30D_WATER__c, Well__r.Well_Type__c FROM FM_Run_Sheet__c WHERE Date__c = :oFoundRunSheet.Date__c AND Facility__c = :oFoundRunSheet.Facility__c ORDER BY Tank__c] : [SELECT Id, Name, Act_Flow_Rate__c, Act_Tank_Level__c, Business_Unit__c, Comments_Category__c, Current_Well_Status__c, Date__c, Equipment_Tank__c, Equipment_Tank__r.Door_Status__c, Facility__c, Flowline_Volume__c, Fluid_Booked__c, Is_Fluid_Facility__c, Load_Type__c, Load_Weight__c, Operating_District__c, Outstanding_Oil__c, Outstanding_Water__c, Sour__c, Standing_Comments__c, Tank__c, Tank_Label__c, Tank_Size__c, Tomorrow_Oil__c, Tomorrow_Water__c, Tonight_Oil__c, Tonight_Water__c, Total_Loads__c, Well_Name_Tank__c, Well__c, Well__r.Hazardous_H2S__c, Well__r.Route__c, Well__r.Route__r.Field_Senior__c, Well__r.Statusicon__c, Well__r.Unit_Configuration__c, Well__r.Current_Well_Status__c, Well__r.PVR_AVGVOL_30D_OIL__c, Well__r.PVR_AVGVOL_30D_SAND__c, Well__r.PVR_AVGVOL_30D_WATER__c, Well__r.Well_Type__c FROM FM_Run_Sheet__c WHERE Date__c = :oFoundRunSheet.Date__c AND Well__c = :oFoundRunSheet.Well__c ORDER BY Tank__c];
			lTankList = [SELECT Id, Name, Door_Last_Pulled_Date__c, Door_Status__c, Equipment__c, Last_Inspection_Date__c, Latest_Tank_Reading_Date__c, Low_Level__c, SCADA_Tank_Level__c, Serial_Number__c, Tank_Label__c, Tank_Settings__c, Tank_Size_m3__c, Tank_Status__c FROM Equipment_Tank__c WHERE Equipment__r.Location__c = :oFoundRunSheet.Well__c];
		}

		// Get List of associated Tanks
		for(Equipment_Tank__c oOneTank : lTankList)
		{
			mapTanks.put(oOneTank.Tank_Label__c, oOneTank);
		}

		List<RunsheetWrapper> lWrappers = new List<RunsheetWrapper>();
		RunsheetWrapper oOneWrapper;

		Integer iCount = 1;

 		for(FM_Run_Sheet__c oOneRunSheet : lRunSheets)
 		{
			oOneWrapper = new RunsheetWrapper(oOneRunSheet, new Location__c(), new Facility__c(), (mapTanks.get(oOneRunSheet.Tank__c) == null) ? new Equipment_Tank__c() : mapTanks.get(oOneRunSheet.Tank__c), this.sRunsheetId, iCount++);
			lWrappers.add(oOneWrapper);
 		}

		return lWrappers;
	}

	///////////////////////////////
	// * RETRIEVE BOOKING DATA * //
	///////////////////////////////

	private void RetrieveBookingData()
	{
		List<AggregateResult> lBookingResult = [SELECT SUM(Act_Flow_Rate__c) Production, SUM(Act_Tank_Level__c) TankLevel, SUM(Tonight_Oil__c), SUM(Tonight_Water__c), SUM(Tomorrow_Oil__c), SUM(Tomorrow_Water__c), SUM(Tank_Size__c) TankInventory, SUM(Flowline_Volume__c) FlowlineVolume, SUM(Fluid_Booked__c) FluidBooked FROM FM_Run_Sheet__c WHERE Date__c = TODAY AND Well__r.Route__c = :this.sRouteId];
		this.oTodayBookingData = new BookingData(lBookingResult);

		lBookingResult = [SELECT SUM(Act_Flow_Rate__c) Production, SUM(Act_Tank_Level__c) TankLevel, SUM(Tonight_Oil__c), SUM(Tonight_Water__c), SUM(Tomorrow_Oil__c), SUM(Tomorrow_Water__c), SUM(Tank_Size__c) TankInventory, SUM(Flowline_Volume__c) FlowlineVolume, SUM(Fluid_Booked__c) FluidBooked FROM FM_Run_Sheet__c WHERE Date__c = YESTERDAY AND Well__r.Route__c = :this.sRouteId];
		this.oYesterdayBookingData = new BookingData(lBookingResult);
	}
}